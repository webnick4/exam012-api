const express = require("express");
const multer = require("multer");
const path = require("path");
const nanoid = require("nanoid");
const config = require("../config");

const auth = require('../middleware/auth');
const Photo = require("../models/Photo");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const router = express.Router();

const createRouter = () => {

  router.get('/', (req, res) => {
    Photo.find()
      .populate('author')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', [auth, upload.single("image")], (req, res) => {
    const photoData = req.body;

    if (req.file) {
      photoData.image = req.file.filename;
    } else {
      photoData.image = null;
    }

    photoData.author = req.user._id;

    const photo = new Photo(photoData);

    photo.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
    Photo.findById(id)
      .populate('author')
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  router.get('/authors/:id', (req, res) => {
    const id = req.params.id;

    Photo.find({author: id})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });


  router.delete('/:id', auth, (req, res) => {
    const id = req.params.id;

    Photo.findOne({_id: id}).then(result => {
      if (!req.user._id.equals(result.author)) {
        res.sendStatus(403);
      } else {
        Photo.findByIdAndRemove(id, (err, doc) => {
          if (err) res.status(404);
          else res.send({ id: doc._id});
        })
          .catch(err => res.status(500).send(err));
      }
    })
  });

  return router;
};

module.exports = createRouter;