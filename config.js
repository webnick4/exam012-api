const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'photo'
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: 5600
  },
  facebook: {
    appId: '685101881881609',
    appSecret: '62d93601f63d93d416a7d6b22cb62ec0'
  }
};

