const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Photo = require('./models/Photo');

mongoose.connect(config.db.url +  '/' + config.db.name);

const db = mongoose.connection;

const collections = ['users', 'photos'];

db.once('open', async () => {
  collections.forEach( async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });


  const [adminUser, user1User, user2User] = await User.create({
    username: 'admin',
    password: '123',
    display_name: 'Administrator',
    role: 'admin'
  }, {
    username: 'john_doe',
    password: '123',
    display_name: 'John Doe',
    role: 'user'
  }, {
    username: 'max_payne',
    password: '123',
    display_name: 'Max Payne',
    role: 'user'
  });

  await Photo.create({
    title: 'Mood',
    author: user1User._id,
    image: 'female.jpg'
  }, {
    title: 'Summer season',
    author: user2User._id,
    image: 'goose.jpg'
  },{
    title: 'Beautiful nature',
    author: user2User._id,
    image: 'nature01.jpg'
  }, {
    title: 'Beautiful nature',
    author: user1User._id,
    image: 'nature03.jpg'
  }, {
    title: 'Beautiful nature',
    author: adminUser._id,
    image: 'nature02.jpg'
  }, {
    title: 'Portrait of a man',
    author: user1User._id,
    image: 'man.jpg'
  });

  db.close();
});